package pl.codementors;

import pl.codementors.model.Copse;
import pl.codementors.model.Ent;
import pl.codementors.model.TreeType;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World");

        Ent ent1 = new Ent(TreeType.LEAF, "Jodła", 200, 21, "Józek" );

        Copse copse = new Copse();
        copse.add(ent1);
        System.out.println("To String");
        System.out.println(ent1.toString());
        //pobieramy enty z pliku
        copse.readEnts("ents.txt");
        //drukujemy wszystki enty z listy
        System.out.println("\nPrint All\n");
        copse.print();
        System.out.println("\nPrint Names\n");
        copse.printNames();
        System.out.println("\nPrint all with info\n");
        copse.printAllInfo();
        System.out.println("\nSize");
        //jeżeli metoda coś zwraca musimy to zsoutować
        System.out.println(copse.count());
        System.out.println("size of Ents in species");
        System.out.println(copse.count("Jabłoń"));
        //zad 8
        System.out.println(copse.count("Jabłoń", TreeType.LEAF));
        //zwraca listę więc możemy łancuchowo ją wypisać
        System.out.println("\nFetch");
        copse.fetch("Jabłoń", "Dąb").forEach(System.out::println);
        //typ i gatunek
        System.out.println("\nFetch with type and specie");
        //znów dodajemy forEach - bo metoda zwraca a nie drukuje sama
        copse.fetch(TreeType.LEAF, "Dąb", "Wisnia").forEach(System.out::println);
        //posortowana lista
        System.out.println("\nSorted fetch");
        copse.fetchSorted().forEach(System.out::println);
        //posortowana wdg wieku lista
        System.out.println("\nFetch with age sort");
        copse.fetchSortedAge().forEach(System.out::println);
        //height and age sorted list
        System.out.println("\nsort wzrost i wiek");
        copse.fetchSortedMore().forEach(System.out::println);
        //wypisuje wszystkie gatunki
        System.out.println("\nAll species");
        copse.fetchSpecies().forEach(System.out::println);
        //posortowana alfabetycznie i bez powtórek
        System.out.println("\nSpecies sorted with no repetition");
        copse.fetchWithDistinct().forEach(System.out::println);
        //wartości wdg wzrostu
        System.out.println("\nHeight values");
        System.out.println("Max " + copse.maxHeight());
        System.out.println("Min " + copse.minHeight());
        System.out.println("Average " + copse.averageHeight());
        //names
        System.out.println("\nNames");
        copse.names().forEach(System.out::println);
        //najdłuższe imię
        System.out.println("\nMax length name");
        System.out.println(copse.maxName());
        //długość imion
        System.out.println("\nNames Length");
        System.out.println(copse.namesLengths());
        //seedlimngs
        System.out.println("Seedlings");
        copse.seedlings().forEach(System.out::println);
    }
}
