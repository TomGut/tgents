package pl.codementors.model;

public class Ent implements Comparable<Ent>{

    private TreeType type;
    private String species;
    private int height;
    private int age;
    private String name;

    @Override
    public String toString() {
        return "Ent{" + "type=" + type + ", species='" + species + '\'' + ", height=" + height + ", age=" + age + ", name='" + name + '\'' + '}';
    }

    public Ent(TreeType type, String species, int height, int age, String name) {
        this.type = type;
        this.species = species;
        this.height = height;
        this.age = age;
        this.name = name;
    }

    public TreeType getType() {
        return type;
    }

    public void setType(TreeType type) {
        this.type = type;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ent ent = (Ent) o;

        if (height != ent.height) return false;
        if (age != ent.age) return false;
        if (type != ent.type) return false;
        if (species != null ? !species.equals(ent.species) : ent.species != null) return false;
        return name != null ? name.equals(ent.name) : ent.name == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (species != null ? species.hashCode() : 0);
        result = 31 * result + height;
        result = 31 * result + age;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Ent o) {
        //najpierw porównujemy po typie
        int ret = type.compareTo(o.type);
        //jeżeli to się zgodzi idzimey dalej
        if(ret == 0){
            ret = species.compareTo(o.species);
        }
        if(ret == 0){
            ret = height - o.height;
        }
        if(ret == 0){
            ret = age - o.age;
        }
        if(ret == 0){
            ret = name.compareTo(o.name);
        }

        return ret;
    }
}
