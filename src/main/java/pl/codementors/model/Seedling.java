package pl.codementors.model;

public class Seedling {

    private String species;
    private TreeType type;

    //w tym konstruktorze pobieramy pola z Enta i ustawiamy wybrane pola w sadzonce
    public Seedling(Ent ent){
        species = ent.getSpecies();
        type = ent.getType();
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public TreeType getType() {
        return type;
    }

    public void setType(TreeType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Seedling{" + "species='" + species + '\'' + ", type=" + type + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Seedling seedling = (Seedling) o;

        if (species != null ? !species.equals(seedling.species) : seedling.species != null) return false;
        return type == seedling.type;
    }

    @Override
    public int hashCode() {
        int result = species != null ? species.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
