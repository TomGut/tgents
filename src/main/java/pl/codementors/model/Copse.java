package pl.codementors.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class Copse {

    private  static  final Logger log = Logger.getLogger(Copse.class.getName());

    private List<Ent> entList = new ArrayList<>();

    public Copse() {

    }

    public void readEnts(String path) {

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {

            while (br.ready()){
                String type = br.readLine();
                String species = br.readLine();
                String height = br.readLine();
                String age = br.readLine();
                String name = br.readLine();
                int heightAsInt = Integer.parseInt(height);
                int ageAsInt = Integer.parseInt(age);
                TreeType typeTree = TreeType.valueOf(type);

                Ent ent = new Ent(typeTree,species, heightAsInt, ageAsInt, name);
                entList.add(ent);
            }
        }catch (IOException ex) {
            //logowanie wyjątku
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    //drukiujemy wszystkie enty
    public void print() {
        entList.forEach(System.out::println);
    }

    public void printNames(){
        entList.stream().map(Ent::getName).forEach(System.out::println);
    }

    //drukowanie danych info - juz bez strumienia bo tak łatwiej
    public void printAllInfo(){
       entList.forEach(e -> System.out.println("Imię: " + e.getName() + " Gatunek: " + e.getSpecies() + " Typ:  " + e.getType()));
    }

    //zliczanie wszystkich entów
    public int count(){
        return entList.size();
    }

    //zwracanie ilości entów w danym gatunku
    public long count(String species){
        return entList.stream().filter(e -> e.getSpecies().equals(species)).count();
    }

    public long count(String species, TreeType type){

        return entList.stream().filter(e -> e.getType().equals(type)).filter(e -> e.getType().equals(species)).count();
    }

    //z nieograniczoną liczba param
    public List<Ent> fetch(String... species){
        //musimy tak zrobić bo te 3 kropki w sygnaturze wskazuja na liste - dodatkowo będziuę można
        // wywołać contains na kolekcji, lista - bo nie wiemy ile będzie parametrów
        List<String> speciesList = Arrays.asList(species);
        return entList.stream().filter(e -> speciesList.contains(e.getSpecies())).collect(Collectors.toList());
    }

    //gatunek i typ
    public List<Ent> fetch(TreeType type, String... species){
        //musimy tak zrobić bo te 3 kropki w sygnaturze wskazuja na liste - dodatkowo będziuę można
        // wywołać contains na kolekcji, lista - bo nie wiemy ile będzie parametrów
        List<String> speciesList = Arrays.asList(species);
        return entList.stream().filter(e -> e.getType().equals(type)).filter(e -> speciesList.contains(e.getSpecies())).collect(Collectors.toList());
    }

    //zwraca posortowaną listę
    public List<Ent> fetchSorted(){
        return entList.stream().sorted().collect(Collectors.toList());
    }

    //sortuje wdg wieku
    public List<Ent> fetchSortedAge(){
        return  entList.stream().sorted((e1, e2) -> e1.getAge() - e2.getAge()).collect(Collectors.toList());
    }

    //sortowanie wiek i wzrost
    public List<Ent> fetchSortedMore(){
        return  entList.stream().sorted(Comparator.comparingInt(Ent::getHeight).thenComparingInt(Ent::getAge)).collect(Collectors.toList());
    }

    //spis gatunków
    public Set<String> fetchSpecies(){
        return entList.stream().map(Ent::getSpecies).collect(Collectors.toSet());
    }

    //spis gatunków bez powtorzen - distinct spr że nie ma powtórzeń, a sorted = sortuje
    // naturalnym porządkiem - alfabetycznie
    public List<String> fetchWithDistinct(){
        return entList.stream().map(Ent::getSpecies).distinct().sorted().collect(Collectors.toList());
    }

    //wysokości
    //max
    public int maxHeight(){
        //or else wrzucamy bo gdyby strumień wyrzucił pustą wartość wyleci wyjątek
        return entList.stream().mapToInt(Ent::getHeight).max().orElse(0);
    }

    //min
    public double minHeight(){
        //or else wrzucamy bo gdyby strumień wyrzucił pustą wartość wyleci wyjątek
        return entList.stream().mapToInt(Ent::getHeight).min().orElse(0);
    }

    //average
    public double averageHeight(){
        //or else wrzucamy bo gdyby strumień wyrzucił pustą wartość wyleci wyjątek
        return entList.stream().mapToInt(Ent::getHeight).average().orElse(0);
    }

    //lista imion
    public List<String> names(){
        return entList.stream().map(Ent::getName).collect(Collectors.toList());
    }

    //najdłuższe imię
    public String maxName(){
        return entList.stream().map(Ent::getName).max(Comparator.comparingInt(String::length)).orElse(null);
    }

    //długość imion
    public List<Integer> namesLengths(){
        return entList.stream().map(Ent::getName).map(String::length).collect(Collectors.toList());
    }

    /*
    //pierwsze litery
    public List<Character> namesFirstChar(){
        return entList.stream().map(Ent::getName).map(n -> n.charAt());
    }

*/
    //lista sadzonek powstałych na bazie entów
    //bierzemy strumień entów
    public List<Seedling> seedlings(){
        return entList.stream().map(Seedling::new).collect(Collectors.toList());
    }

    //dodawanie entów do listy
    public void add(Ent ent){
        entList.add(ent);
    }

}
