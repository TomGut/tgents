package pl.codementors.model;

public enum TreeType {

    LEAF,
    SOFTWOOD
}
